Date: 20161022

Modify the student list we programmed in class to include the following  

  1. Structure containing student's information (Name,ID, Age, GPA)  
  2. Class called StudentList which is a linked list to hold the structure in step 1. The class must have  
     a. AddNew - to add new student to the list  
     b. Remove - to remove a student from the list  
     c. Display - to show student information when given student ID  
     d. Save - to save all students information in the list to file  
  3. Your program must have 3 files as demonstrated in class  
  4. Your program must read students' information from a file and save students' list back to the file  
  5. Your program cannot use vector or array to store information. You must use your own linked list as shown in class  