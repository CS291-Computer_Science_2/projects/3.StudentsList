////////////////////////////////////////////////////////////////////////////////////
// Name: Jose Madureira
// Date: 2016-10-22
// File Purpose: contain method definitions for modifying a list
////////////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <fstream>
#include <sstream>
#include <limits>
#include <string>
//#include <sstream>
#include "headers.h"
using namespace std;

/*COnstructor Initialization*/
StudentList::StudentList(const string &fln) :fname(fln) {
	/*StudentList*/
	head= nullptr;
	readFile(head);
}

/*Destructor*/
StudentList::~StudentList() {
	/*Delete List Nodes*/
	if(head==nullptr) return;
	else {
		while(head!= nullptr) {	//While head adress contains a valid memory location
			stud *tmp;
			tmp= head;			//  assign top node to a temporary list
			head= head->next;	//	proceed to next node
			delete tmp;			//  erase each node as it moves along
		}
	}
}

void StudentList::readFile(stud *head) {
	ifstream ifs(fname);
	if(!ifs) {
		printf("\n\t /!\\ Can not Open File \"%s\" /!\\ \n",fname.c_str());
		return;

	} else {
		string line="";
		int ktr=0;

		while(ifs) {
			if(ktr==0) getline(ifs,line); //ignore row with headers
			else {
				string tid,tname,tage,tgpa;
				getline(ifs,tid,',');
				getline(ifs,tname,',');
				getline(ifs,tage,',');
				getline(ifs,tgpa);

				if(!ifs) break;	//kill loop after last element in file is fetched;
				head= stacker(tid,tname,tage,tgpa);	//build new node
			}
			ktr++;
		}
		printf("\n Successfully Loaded data from \"%s\"",fname.c_str());
	}
	ifs.close();
}

void StudentList::Save() {
	/*Fetch header row in file before rewrite*/
	string hdr;
	ifstream ifs(fname);
	getline(ifs,hdr,'\n');
	ifs.close();

	/*Write changes to file*/
	ofstream ofs(fname);
	if(!ofs) {
		printf("\n\t /!\\ Can not Open File \"%s\" /!\\ \n",fname.c_str());
		return;
	} else {
		char c=',';
		ofs<<hdr<<endl;
		for(stud *temp= head; temp; temp=temp->next) {
			ofs<<temp->id<<c<<temp->name<<c<<temp->age<<c<<temp->gpa<<endl;
		}
		printf("\n Successfully saved data to \"%s\"",fname.c_str());
	}
	ofs.close();
}

stud* StudentList::stacker(string id, string name, string age, string gpa) {
	// concept idea from Professor Sunmonu - InventorySample
	// implementation from http://tinyurl.com/k6e6zqe

	stud *tmp= new stud;
	if(tmp==nullptr) printf("\n\t\tStack is full.\n");

	tmp->id= stoi(id);
	tmp->name= name;
	tmp->age= stoi(age);
	tmp->gpa= stof(gpa);

	if(head== nullptr) {
		head= tmp;
		head->next=nullptr;
	} else {
		tmp->next= head;
		head= tmp;
	}
}

void StudentList::AddNew() {
	string anId,anAge,anName,anGpa;
	cin.get();
	cout<<">>>ID: ";
	getline(cin,anId);
	cout<<">>>Name: ";
	getline(cin,anName);
	cout<<">>>Age: ";
	getline(cin,anAge);
	cout<<">>>GPA: ";
	getline(cin,anGpa);
	stacker(anId,anName,anAge,anGpa);
}

void StudentList::Remove() {
	/*The method I build from scratch deleted everything past the node I wanted*/	
	/*Using "Masong's" method from http://tinyurl.com/j4gfgwl
	    All the comments belong to Masong.*/
	
	int idNr=0;
	printf("\n  Enter ID number: ");
	cin>>idNr;

	stud *pPre= nullptr, *pDel= nullptr;

	/* Check whether it is the head node?
	 if it is, delete and update the head node */
	if (head->next->id == idNr) {
		/* point to the node to be deleted */
		pDel = head;
		/* update */
		head = pDel->next->next;
		delete pDel;
		return;
	}

	pPre = head;
	pDel = head->next;

	printf("\n\t\tID '%d' Was Not Found\n",idNr);
}

/*Display Student info by ID number*/
void StudentList::Display() {
	int idNr=0;
	printf("\n  Enter ID number: ");
	cin>>idNr;

	stud* pupil;
	pupil= head;

	while(pupil != nullptr) {
		if(pupil->id == idNr) {
			/*Pretty formatting using 'rpt' method from tools.cpp*/
			cout<<rpt(57,'-');
			cout<<"\n      ID"<<rpt(7,' ','|')<<rpt(9,' ')<<"Name"<<rpt(14,' ','|')
			    <<" Age |"<<" GPA  |\n";
			cout<<rpt(15,'-','|')<<rpt(27,'-','|')<<rpt(5,'-','|')<<rpt(6,'-','|');
			printf("\n  %-12d | %-25s | %-3d | %-3.2f |\n",pupil->id,(pupil->name).c_str(),pupil->age,
			       pupil->gpa);
			cout<<rpt(57,'-');
			return;
		}
		pupil= pupil->next;
	}
	printf("\n\t\tID '%d' Was Not Found\n",idNr);
}

/*FOR TESTING PROPER LIST STORAGE*/
void StudentList::DisplayAll() {
//	Jonny Henly  http://stackoverflow.com/a/21894024/6793751
	stud* tmp = head;

	cout<<"\n      ID"<<rpt(7,' ','|')<<rpt(12,' ')<<"Name"<<rpt(11,' ','|')<<" Age |"<<" GPA  |"<<endl;
	cout<<rpt(15,'-','|')<<rpt(27,'-','|')<<rpt(5,'-','|')<<rpt(6,'-','|');

	while(tmp->next != nullptr) {
		tmp = tmp->next;
		printf("\n  %-12d | %-25s | %-3d | %-3.2f |",tmp->id,(tmp->name).c_str(),tmp->age,tmp->gpa);
	}
}
