///////////////////////////////////////////////////////////////////
// Name: Jose Madureira
// Date: 2016-10-22
// File Purpose:
//////////////////////////////////////////////////////////////////
#include <iostream>
#include <limits>
using namespace std;

/*Fix broken cin*/
extern void cinFix() {	//defined in headers.h
	cin.clear();
	cin.ignore(numeric_limits<streamsize>::max(), '\n');
	cout << "\nInvalid input.  Try again: ";
	cin.get();
}

//Number, Char to repeat, Optional post-repetition char
extern string rpt(int nr,char rep,char end='j') {
	if(end=='j') return string(nr,rep);
	else return string(nr,rep)+string(1,end);
}

