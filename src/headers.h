/////////////////////////////////////////////////////////////////////////////
// Name: Jose Madureira
// Date: 2016-10-22
// File Purpose: Hold all function prototypes, along with externs from tools.cpp
/////////////////////////////////////////////////////////////////////////////
#ifndef HEADERS_H
#define HEADERS_H
using namespace std;

/*Node Structure*/
struct stud {
	int id;
	string name;
	int age;
	float gpa;
	stud *next;
};

/*tools.cpp EXTERNALS*/
string rpt(int nr,char rep,char end='j'); //Allow long character repetitions
void cinFix();	//Fix cin errors

/*Methods to operate on a linked list*/
class StudentList {
	public:
		StudentList(const string &fln);
		~StudentList();
		void AddNew();
		void Remove();
		void Display();
		void Save();

		void DisplayAll(); //Test displaying all students
	private:
		void readFile(stud *head);
		stud *stacker(string id, string name, string age, string gpa);
		stud *head;
//		void tailStacker(int id, string name, int age, float gpa);
		const string fname;
};
#endif
