////////////////////////////////////////////////////////////////////////////////////////////
//  Name: Jose Madureira	CS291-Fall 2016
//	Date: 2016-10-22
//	Project: Project 3 - Students List
//  Pseudo Code: (1) Read data from a file
//				(2) Store data from file to a linked list
//				(3) Display stored data to user
//				(4) Add user entered data to a linked list
//				(5)	Allow user to delete data in the linked list
//				(6) Save changes to a file
////////////////////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <string>
#include <limits>
#include "headers.h"
using namespace std;

void menu(int *choice);

int main() {
	string dbFile="studentDB.csv";
	printf("\n\t\t#---------------------------------------#");
	printf("\n\t\t### Student Records Management System ###");
	printf("\n\t\t#---------------------------------------#\n");

	StudentList *sl = new StudentList(dbFile); //methods for operating on linked list
	stud *ll= new stud;	//store linked list
	int ch;

//	sl->Display();


	do {
		printf("\n\n [Main Menu] ");
		printf("\n    1. Add new student");
		printf("\n    2. Remove student");
		printf("\n    3. View student record (by ID)");
		printf("\n    4. Save Changes");
		printf("\n    5. Quit");
		do  {
			cout<<"\n\n>>You Choose: ";
			cin >> ch;
			if(cin.good()) break;
			while(!(cin.good())) {
				cinFix();
				cin>>ch;
			}
		} while(ch<1 || ch>5);

		//Execute Menu Option
		switch (ch) {
			case 1:
				sl->AddNew();
				break;
			case 2:
				sl->Remove();
				break;
			case 3:
				sl->Display();
				break;
			case 4:
				sl->Save();
				break;
			case 5:
				break;
			default:
				cout<<"\n\t\t/!\\Invalid Menu Selection/!\\\n";
		}
	} while(ch!=5);


	cout<<"\n\t\tGoodbye!\n\t";
	system("pause");
	return 0;
}

